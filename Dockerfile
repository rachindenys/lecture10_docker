    FROM node:10.15.3-alpine
    WORKDIR .
    COPY package*.json ./
    RUN npm install \ 
        && npm install typescript -g
    COPY . ./
    RUN npm run build

    CMD ["npm", "run", "start"]
        