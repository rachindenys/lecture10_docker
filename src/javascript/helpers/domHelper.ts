import IElementInterface from "../Interfaces/IElementInterface";

export function createElement({ tagName, className, attributes = {} } : IElementInterface) : HTMLElement {
    const element : HTMLElement = document.createElement(tagName);
    
    if (className) {
      element.classList.add(className);
    }
  
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
    return element;
  }

export function uncheckCheckboxes(): void{
  const checkboxes: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');
  Array.prototype.forEach.call(checkboxes, (checkbox: HTMLInputElement) => {
    checkbox.checked = false;
  })
}