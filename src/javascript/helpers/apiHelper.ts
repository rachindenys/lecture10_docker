import CrudEnum from '../Interfaces/CrudEnum';
import IFighterDetailsInterface from '../Interfaces/IFighterDetailsInterface';
import IFighterInterface from '../Interfaces/IFighterInterface';
import { fightersDetails, fighters } from './mockData';

const API_URL : string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
export const useMockAPI : boolean = true;

async function callApi(endpoint : string, method: CrudEnum): Promise<IFighterDetailsInterface | IFighterInterface[]> {
  const url : string = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(result.content))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<IFighterDetailsInterface | IFighterInterface[]> {
  const response: IFighterInterface[] | IFighterDetailsInterface = endpoint === 'fighters.json' ? <IFighterInterface[]>fighters : getFighterById(endpoint);
  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighterDetailsInterface {
  const start : number = endpoint.lastIndexOf('/');
  const end : number = endpoint.lastIndexOf('.json');
  const id : string = endpoint.substring(start + 1, end);

  const fighter: IFighterDetailsInterface = <IFighterDetailsInterface>fightersDetails.find((it) => it._id === id);
  return fighter;
}

export { callApi };
