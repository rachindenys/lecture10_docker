import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement, uncheckCheckboxes } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import IFighterDetailsInterface from './Interfaces/IFighterDetailsInterface';
import { getFighterDetails } from "./services/fightersService";
import IFighterInterface from './Interfaces/IFighterInterface';

export function createFighters(fighters: IFighterInterface[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements: HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer: HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: Event, fighter: IFighterInterface): Promise<void> {
  const fullInfo: IFighterDetailsInterface = <IFighterDetailsInterface>await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string) : Promise<IFighterInterface> {
    return await getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetailsInterface>();

  return async function selectFighterForBattle(event: Event, fighter: IFighterInterface): Promise<void> {
    const fullInfo: IFighterInterface = <IFighterInterface>await getFighterInfo(fighter._id);
    const eventTarget: HTMLInputElement = event.target as HTMLInputElement;
    if (eventTarget.checked) {
      selectedFighters.set(fighter._id, <IFighterDetailsInterface>fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
        var fighters: IFighterDetailsInterface[] = [];
        for(let fighter of selectedFighters.values()){
            fighters.push(fighter);
        }
      uncheckCheckboxes();
      const winner: IFighterDetailsInterface = await fight(fighters[0], fighters[1]);
      showWinnerModal(winner);
      selectedFighters.clear();
    }
  }
}
