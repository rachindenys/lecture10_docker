import { createElement } from '../helpers/domHelper';
import IDictionary from '../Interfaces/IDictionary';
import IFighterDetailsInterface from '../Interfaces/IFighterDetailsInterface';
import { showModal } from './modal';

export  function showFightModal(fighter1 : IFighterDetailsInterface, fighter2: IFighterDetailsInterface, damage: number, isFirstAttacks: boolean): void {
  const title : string = 'FIGHT!';
  const bodyElement: HTMLElement = createFightModal(fighter1, fighter2, damage, isFirstAttacks);
  showModal({ title, bodyElement });
}

function createFightModal(fighter1 : IFighterDetailsInterface, fighter2: IFighterDetailsInterface, damage: number, isFirstAttacks: boolean) : HTMLElement {

  const fighterModal1: HTMLElement = createFighterModal(fighter1, isFirstAttacks, damage);
  const fighterModal2: HTMLElement = createFighterModal(fighter2, !isFirstAttacks, damage);
  const fightModal : HTMLElement = createElement({ tagName: 'div', className: 'modal-body-fight' });
  fightModal.append(fighterModal1);
  fightModal.append(fighterModal2);
  return fightModal;
}

function createFighterModal(fighter: IFighterDetailsInterface, isAttacker: boolean, damage: number): HTMLElement{
    const { name, attack, defense, health, source } = fighter;
    const fighterDetailsList : HTMLElement = createElement({ tagName: 'ul', className: 'details-list' });
    const nameElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-name' });
    const attackElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-attack' });
    const defenseElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-defense' });
    const healthElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-health' });
    const attributes: IDictionary = { src: source };
    const figureElement: HTMLElement = createElement({ tagName: 'figure' });
    const imageElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
    const figCaptionElement: HTMLElement = createElement({ tagName: 'figcaption' });
    nameElement.innerText = 'Name: ' + name;
    attackElement.innerText = 'Attack: ' + attack;
    defenseElement.innerText = 'Defense: ' + defense;
    healthElement.innerText = 'Health: ' + health.toFixed(3);
    imageElement.innerText = source;
    figCaptionElement.innerText = isAttacker ? 'Attacker' : 'Defender';
    fighterDetailsList.append(nameElement);
    fighterDetailsList.append(attackElement);
    fighterDetailsList.append(defenseElement);
    fighterDetailsList.append(healthElement);
    figureElement.append(imageElement);
    figureElement.append(figCaptionElement);
    fighterDetailsList.append(figureElement);
    if (!isAttacker){
        if(damage <= 0){
            const givenDamageElement: HTMLElement = createElement( { tagName: 'div', className: 'fighter-dodge-damage'});
            damage = 0.0000;
            givenDamageElement.innerText = "DAMAGE GIVEN: " + damage.toFixed(4) + "\n(Dodged)";
            fighterDetailsList.append(givenDamageElement);   
        }
        else{
            const givenDamageElement: HTMLElement = createElement( { tagName: 'div', className: 'fighter-given-damage'});
            givenDamageElement.innerText = "DAMAGE GIVEN: " + damage.toFixed(4);
            fighterDetailsList.append(givenDamageElement);   
        }
    }
    return fighterDetailsList;
}

export function hideFightModal() {
    const modal: Element = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
  }