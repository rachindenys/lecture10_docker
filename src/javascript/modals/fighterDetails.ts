import { createElement } from '../helpers/domHelper';
import IDictionary from '../Interfaces/IDictionary';
import IFighterDetailsInterface from '../Interfaces/IFighterDetailsInterface';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter : IFighterDetailsInterface): void {
  const title : string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter : IFighterDetailsInterface) : HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails : HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterDetailsList : HTMLElement = createElement({ tagName: 'ul', className: 'details-list' });
  const nameElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-name' });
  
  // show fighter name, attack, defense, health, image
  const attackElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-attack' });
  const defenseElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-defense' });
  const healthElement : HTMLElement = createElement({ tagName: 'li', className: 'fighter-health' });
  const attributes: IDictionary = { src: source };
  const imageElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  nameElement.innerText = 'Name: ' + name;
  attackElement.innerText = 'Attack: ' + attack;
  defenseElement.innerText = 'Defense: ' + defense;
  healthElement.innerText = 'Health: ' + health;
  imageElement.innerText = source;
  fighterDetailsList.append(nameElement);
  fighterDetailsList.append(attackElement);
  fighterDetailsList.append(defenseElement);
  fighterDetailsList.append(healthElement);
  fighterDetails.append(fighterDetailsList);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
