interface IModalInterface{
    title: string, 
    bodyElement: HTMLElement
}

export default IModalInterface;