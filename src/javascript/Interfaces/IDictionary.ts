interface IDictionary{
    [index: string]: string
}

export default IDictionary;